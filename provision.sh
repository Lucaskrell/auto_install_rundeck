#!/bin/bash

VM=$1
if [ "$VM" = "rundeck" ];then
    echo "ce qui est ici sera exécuté sur rundeck"
    sudo apt-get update
    sudo apt install -y curl gnupg    
    echo "deb http://deb.debian.org/debian stretch main" | sudo tee -a /etc/apt/sources.list
    echo "deb https://rundeck.bintray.com/rundeck-deb /" | sudo tee -a /etc/apt/sources.list
    echo "deb https://dl.bintray.com/rundeck/rundeck-deb /" | sudo tee -a /etc/apt/sources.list
    curl https://bintray.com/user/downloadSubjectPublicKey?username=bintray > key.key
    sudo apt-key add key.key
    rm key.key 
    sudo apt-get update
    sudo apt install -y openjdk-8-jdk rundeck chrony rundeck-cli    
    sudo cp /vagrant/rundeck/sudoers /etc/sudoers
    sudo chown root:root /etc/sudoers
    sudo cp /vagrant/rundeck/framework.properties /etc/rundeck/framework.properties
    sudo chown rundeck:rundeck /etc/rundeck/framework.properties
    sudo cp /vagrant/rundeck/rundeck-config.properties /etc/rundeck/rundeck-config.properties
    sudo chown rundeck:rundeck /etc/rundeck/rundeck-config.properties
    sudo cp /vagrant/rundeck/rundeckd /etc/default/rundeckd
    sudo chown root:root /etc/default/rundeckd
    sudo cp /vagrant/rundeck/realm.properties /etc/rundeck/realm.properties
    sudo chown rundeck:rundeck /etc/rundeck/realm.properties
    sudo update-rc.d rundeckd defaults
    sudo cp /vagrant/rundeck/nodes.xml /home/vagrant/nodes.xml
    sudo chown vagrant:vagrant /home/vagrant/nodes.xml
    sudo cp /vagrant/rundeck/chrony.conf /etc/chrony/chrony.conf
    sudo chown root:root /etc/chrony/chrony.conf
    ssh-keygen -N "" -m pem -f /home/vagrant/.ssh/id_rsa
    sudo chown vagrant:vagrant /home/vagrant/.ssh/id_rsa
    sudo chmod +r /home/vagrant/.ssh/id_rsa
    sudo cp /home/vagrant/.ssh/id_rsa.pub /vagrant/rundeck/id_rsa.pub
    export RD_URL=http://localhost:4444
    export RD_USER=admin
    export RD_PASSWORD=rundeck_ptut
    echo "export RD_URL=http://localhost:4444\nexport RD_USER=admin\nexport RD_PASSWORD=rundeck_ptut" >> ~/.profile
    sudo service rundeckd start
    bool=true
    while [ $bool ]; do
	    sleep 10
	    tmp=$(sudo tail -n1 /var/log/rundeck/service.log);
	    if [ "$tmp" == "Grails application running at http://localhost:4444 in environment: production" ]; then
    		rd projects create -f /vagrant/rundeck/project_conf -p testprojet
		rd keys create -t privateKey -f /home/vagrant/.ssh/id_rsa -p keys/rundeck
		rd jobs load -f /vagrant/rundeck/job.xml -p testprojet
		$bool=false
		break
	    fi
    done
    sudo reboot now 
fi

if [ "$VM" = "web" ];then
    echo "ce qui est ici sera exécuté sur web"
    sudo apt-get update
    sudo apt install -y curl apache2 php mariadb-server php-mysql chrony
    sudo cp /vagrant/web/production.conf /etc/apache2/sites-available/production.conf
    sudo cp /vagrant/web/test.conf /etc/apache2/sites-available/test.conf
    sudo chown root:root /etc/apache2/sites-available/production.conf
    sudo chown root:root /etc/apache2/sites-available/test.conf
    sudo mkdir /var/www/test/
    sudo mkdir /var/www/production/
    sudo chown vagrant:vagrant /var/www/test/
    sudo chown root:root /var/www/production/
    sudo a2enmod rewrite
    sudo a2enmod php7.3
    sudo a2ensite test
    sudo a2ensite production
    sudo systemctl reload apache2
    sudo mysql --execute="CREATE DATABASE test;CREATE DATABASE production; CREATE USER 'dev'@'localhost' IDENTIFIED BY 'dev'; GRANT ALL ON test.* TO 'dev'@'localhost'; FLUSH PRIVILEGES;"
    sudo cp /vagrant/web/chrony.conf /etc/chrony/chrony.conf
    sudo chown root:root /etc/chrony/chrony.conf
    cat /vagrant/rundeck/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
    sudo reboot now
fi
