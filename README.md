Téléchargez ces fichiers, placez vous dans le dossier où vous avez placé ces fichiers puis exécutez la commande vagrant up.

Patientez jusqu’à la fin de l’installation de tout ce qui est nécessaire. Lorsque les deux machines ont été créées et provisionnées par vagrant, vous pouvez vous connecter à chacune d'elles via la commande vagrant ssh [web|rundeck].

Pour vous connecter à l’interface graphique de Rundeck, accéder via un navigateur web à l’adresse : http://localhost:4444.

Vous verrez apparaître le formulaire de connexion à Rundeck. Vous pouvez vous connecter grâce à l'identifiant “admin” et au mot de passe “rundeck_ptut”.

Vous vous trouvez maintenant sur l’interface de gestion de Rundeck.
Un projet ainsi que trois jobs ont déjà été créés pour vous afin de vous donner un exemple des possibilités qu’offre Rundeck.
